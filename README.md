::::::::::::::::::::::::::::::::::::::::::
::::::::: Backend Task To-do's :::::::::::
::::::::::::::::::::::::::::::::::::::::::

-
-
-

## TEACHER

[x] GET /teachers

[x] POST /teachers/:id

[x] PUT /teachers/:id

[x] DELETE /teacher/:id

## Create a subject

[x] POST /subjects

## Get all subject in all grade levels

[x] GET /subjects

## Delete a subject by ID

[x] DELETE /subjects/:id

## Update a subject

[x] PUT /subjects/:id

## Get all subjects for grade-[:level] level with specific day

[x] GET /subjects/grade/:level

Body:

{
tag: "monday"
}

## Get all subjects for the week for grade-[:level]

[x] GET /subjects/grade/:level/all

## Mock a day to get the subjects of a particular day

## [x] GET /subjects/grade/:level/mock/:day

-
-
- ::::::::::::::::::::::::::::::::::::::::::
  ::::::::: Front-end Task To-do's :::::::::
  ::::::::::::::::::::::::::::::::::::::::::
-
-
-

## Teacher

[x] UI List Teachers
[x] Edit UI List Teachers
[ ] Function edit list teacher

## Subject

[ ] Add UI Create Teacher
[ ] Edit UI Create Teacher

## Mock DATA

https://mockaroo.com/
https://stackoverflow.com/questions/30593185/setting-up-fake-data-in-mongodb-for-testing

## Node / Express Boilerplate Reference

How to implement JWT authentication in Express.js app ?

- https://www.geeksforgeeks.org/how-to-implement-jwt-authentication-in-express-js-app/

How to Build an Authentication API with JWT Token in Node.js

- https://www.section.io/engineering-education/how-to-build-authentication-api-with-jwt-token-in-nodejs/

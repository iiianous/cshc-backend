import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import bodyParser from "body-parser";
const app = express();

app.use(express.json());
app.use(cors());
app.use(bodyParser.json());
mongoose.set("useFindAndModify", false);

const connectProps = {
	db: "mongodb://localhost/cshc",
	options: {
		useCreateIndex: true,
		useNewUrlParser: true,
		useUnifiedTopology: true,
	},
	port: 5000,
};

const connectServer = async ({ db, options }) => {
	try {
		await mongoose.connect(db, options);
	} catch (error) {
		console.log("Connection failed!");
	}
};
connectServer(connectProps);

import subjects from "./routes/subjects.js";
import teachers from "./routes/teachers.js";

app.use("/api/subjects", subjects);
app.use("/api/teachers", teachers);

const port = process.env.port || connectProps.port;
app.listen(port, () => console.log(`connected to ${port}`));

import express from "express";
const router = express.Router();

import {
  getAllSubjectsController,
  getAllGradeLevelSubjectsController,
  createSubjectController,
  deleteSubjectController,
  updateSubjectController
} from "../controllers/subject.controller.js";

import { validateTeacher, Teacher } from "../models/teachers.js";
import { validateSubject, Subject } from "../models/subjects.js";

router.get("/", getAllSubjectsController);
router.post("/", createSubjectController);
router.delete("/:id", deleteSubjectController);
router.put("/:id", updateSubjectController);

router.get("/grade/:level", getAllGradeLevelSubjectsController);
router.get("/grade/:level/mock/:slug", async (req, res) => {
	let tag = "";
	console.log(req.params.slug);

	if (req.params.slug === "monday" || "tuesday") {
		tag = req.params.slug;
	}

	if (!req.params.slug) {
		tag = req.params.slug;
	}

	try {
		const subjects = await Subject.find().and([
			{ grade: req.params.level },
			{ tag },
		]);

		res.status(200).send(subjects);
	} catch (error) {
		console.log("Error mocking a day");
	}
});

router.get("/grade/:level/all/", async (req, res) => {
	try {
		const subjects = await Subject.find({ grade: req.params.level }).populate(
			"teacher",
			"-_id -__v -date_added"
		);

		res.status(200).send(subjects);
	} catch (error) {
		console.log("Error getting all subjects");
	}
});

export default router;

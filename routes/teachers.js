import express from "express";
const router = express.Router();

import {
  updateTeacherController,
  getAllTeacherController,
  createTeacherController,
  deleteTeacherController
 } from "../controllers/teacher.controller.js";
import { Teacher, validateTeacher } from "../models/teachers.js"

router.get("/", getAllTeacherController);
router.post("/", createTeacherController);
router.put("/:id", updateTeacherController);
router.delete("/:id", deleteTeacherController);

export default router;

import { Subject, validateSubject } from "../models/subjects.js";
import { Teacher, validateTeacher } from "../models/teachers.js";

export const getAllSubjectsController = async (req, res) => {
	try {
		const subjects = await Subject.find().populate("teacher", "name");

		res.status(200).send(subjects);
	} catch (error) {
		console.log("Error getting all subjects");
	}
};

export const createSubjectController = async (req, res) => {
	const { error } = validateSubject(req.body);
	if (error) return res.status(400).send(error.details[0].message);

  // Check if Subject name is already created
  let subject = await Subject.find({ subject: req.body.subject });
  const item = subject.filter((item) => item.subject === req.body.subject);
  if (item.length) return res.status(409).send("Oops!.. Subject name is already created perhaps just edit it.")

  // Teacher should be created first
	let teacher = await Teacher.findById(req.body.teacher);
	if (!teacher) return res.status(400).send("Create Teacher Profile first.");

	const newSubject = new Subject({
		...req.body,
		teacher: {
			name: teacher.name,
			_id: teacher._id,
		},
	});

  if (newSubject.subject)
	console.log("newSubject", newSubject);

	try {
		const result = await newSubject.save();
		res.status(200).send(result);
	} catch (error) {
		console.log("Error creating subject");
	}
};

export const deleteSubjectController = async (req, res) => {
	try {
		const subject = await Subject.findOneAndRemove({ _id: req.params.id });
		res.status(200).send(subject);
	} catch (error) {
		console.log("Error deleting subject");
	}
};

export const updateSubjectController = async (req, res) => {
	try {
		const updatedSubject = await Subject.findOneAndUpdate(
			{ _id: req.params.id },
			{ $set: req.body },
			{ new: true }
		);
		res.status(200).send(updatedSubject);
	} catch (error) {
		console.log("Error updating subject");
	}
};

export const getAllGradeLevelSubjectsController = async (req, res) => {
	try {
		// const subjects = await Subject.find({ tag: req.body.tag !== "" ? req.body.tag : '' });
		const subjects = await Subject.find({ grade: req.params.level }).populate("teacher", "name");
		const sortedSubjects = subjects.sort((a, b) => {
			let aa = moment(a.start_time, "HHmm").format("HH:mm");
			let bb = moment(b.start_time, "HHmm").format("HH:mm");

			if (aa < bb) {
				return -1;
			}

			if (aa > bb) {
				return 1;
			}

			return 0;
		});

		res.status(200).send(sortedSubjects);
	} catch (error) {
		console.log("Error getting all subjects");
	}
};
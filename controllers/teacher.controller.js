import { Teacher, validateTeacher } from "../models/teachers.js";

export const getAllTeacherController = async (req, res) => {
	try {
		const result = await Teacher.find();
		res.status(200).send(result);
	} catch (error) {
		console.log("Error getting all teachers");
	}
};

export const createTeacherController = async (req, res) => {
	const { errors } = validateTeacher(req.body);

	if (errors) return res.status(400).send(error.details[0].message);

	try {
		const teacher = await new Teacher({ ...req.body });
		const result = await teacher.save();
		res.status(200).send(result);
	} catch (error) {
		console.log("Error creating teacher");
	}
};

export const deleteTeacherController = async (req, res) => {
	try {
		const teacher = await Teacher.findOneAndRemove({ _id: req.params.id });
		res.status(200).send(teacher);
	} catch (error) {
		console.log("Error deleting teacher");
	}
};

export const updateTeacherController = async (req, res) => {
	try {
		const result = await Teacher.findById(req.params.id);

    if (!!req.body.link) {
      result.links.push(req.body.link);
    }

		result.save();
		res.status(200).send(result);
	} catch (error) {
		console.log("Error updating teacher");
	}
};
import mongoose from "mongoose";
import Joi from "@hapi/joi";

const teacherSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	link_default: {
		type: String,
	},
	date_added: {
		type: Date,
		default: Date.now,
	},
  links: {
		type: [String],
		required: false,
	},
  link: {
		type: String,
		required: false,
  }
});

const Teacher = mongoose.model("Teacher", teacherSchema);

const validateTeacher = (teacher) => {
	const schema = Joi.object({
		name: Joi.string().min(2).max(255),
		link_default: Joi.string().min(2).max(255),
		links: Joi.array().items(Joi.string()),
    link: Joi.string().min(2).max(255),
		// meet_link: Joi.string().min(2).max(255),
	});

	return schema.validate(teacher);
};

export { teacherSchema, validateTeacher, Teacher }

import mongoose from "mongoose";
const Schema = mongoose.Schema;
import { Teacher } from "./teachers.js";
import Joi from "@hapi/joi";

const subjectsSchema = new mongoose.Schema({
	subject: String,
	grade: String,
	start_time: String,
	end_time: String,
	teacher: {
		type: Schema.Types.ObjectId,
		required: true,
		ref: Teacher,
	},
	tag: {
		type: String,
		enum: ["monday", "tuesday"],
	},
	date_added: {
		type: Date,
		default: Date.now,
	},
});

const Subject = mongoose.model("Subject", subjectsSchema);

const validateSubject = (subject) => {
	const schema = Joi.object({
		subject: Joi.string().min(3).max(255),
		grade: Joi.number(),
		start_time: Joi.string(),
		end_time: Joi.string(),
		teacher: Joi.string(),
		tag: Joi.valid("monday", "tuesday"),
	});

	return schema.validate(subject);
};

export { validateSubject, Subject, subjectsSchema }
